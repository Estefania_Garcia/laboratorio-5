class Mapa{

    miVisor;
    mapaBase;
    posicionInicial;
    escalaInicial;
    proveedorURL;
    atributosProveedor;
    marcadores=[];
    circulos=[];
    poligono=[];

    constructor(){

        this.posicionInicial=[4.408179, -73.951206];
        this.escalaInicial=15;
        this.proveedorURL='https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png';
        this.atributosProveedor={
            maxZoom:20
        };


        this.miVisor=L.map("mapid");
        this.miVisor.setView(this.posicionInicial,this.escalaInicial);
        this.mapaBase=L.tileLayer(this.proveedorURL,this.atributosProveedor);
        this.mapaBase.addTo(this.miVisor);
    }


    colocarMarcador(posicion){

        this.marcadores.push(L.marker(posicion));
        this.marcadores[this.marcadores.length-1].addTo(this.miVisor);
    }

    colocarCirculo(posicion, configuracion){

        this.circulos.push(L.circle(posicion, configuracion));
        this.circulos[this.circulos.length-1].addTo(this.miVisor);

    }
    colocarPoligono (posicion)
    {
        this.poligono.push(L.polygon(posicion));
        this.poligono[this.poligono.length-1].addTo(this.miVisor);
    }


}

let miMapa=new Mapa();

miMapa.colocarMarcador([4.408179, -73.951206]);
miMapa.colocarCirculo([4.405462, -73.956313], 
{
    color: 'red',
    fillColor: '#f03',
    fillOpacity: 0.5,
    radius: 300
});

miMapa.colocarPoligono([
    [4.403429, -73.945638],
    [4.401611, -73.948631],
    [4.400798, -73.946528]   
])








